﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockSpawner : MonoBehaviour
{

    public Transform spawnPoint;
    public GameObject blockPrefab;

    public float timeBetweenWaves = 1f;

    private float timeToSpawn = 1f;

    void Update()
    {

        if (Time.time >= timeToSpawn)
        {

            SpawnBlocks();
            timeToSpawn = Time.time + Random.Range(2f,4f);

        }

    }

    void SpawnBlocks()
    {
        Instantiate(blockPrefab, spawnPoint.position, Quaternion.identity);
    }
}
