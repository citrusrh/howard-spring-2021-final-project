﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : MonoBehaviour
{

    private SpriteRenderer sprite;
    private Rigidbody2D rb;

    private float g_scale;

    private string color;
    private string[] colors = { "Red", "Blue", "Yellow" };

    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        color = colors[Random.Range(0, colors.Length)];

        g_scale = GameObject.Find("GameManager").GetComponent<GameManager>().g_scale;
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = g_scale;

        if (color == "Red")
        {
            sprite.color = Color.red;
        }
        if (color == "Blue")
        {
            sprite.color = Color.blue;
        }
        if (color == "Yellow")
        {
            sprite.color = Color.yellow;
        }

        this.gameObject.tag = color;
    }

    void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Ground")
        {
            this.gameObject.tag = "Ground";
            rb.bodyType = RigidbodyType2D.Static;
        }
        else if (collision.gameObject.tag == this.gameObject.tag)
        {
            Destroy(this.gameObject);
        }

    }
}
