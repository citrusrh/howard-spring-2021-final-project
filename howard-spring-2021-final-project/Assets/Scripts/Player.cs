﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : MonoBehaviour
{

    public float speed = 10f;
    public float jump = 10f;
    public float mapWidth = 5f;

    public GameObject container;
    public TextMeshProUGUI tmpUGUI;

    private Rigidbody2D rb;
    private SpriteRenderer sprite;
    private BoxCollider2D box;

    private bool canJump = true;
    private string color;

    void Start()
    {

        sprite = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        box = GetComponent<BoxCollider2D>();

    }

    void FixedUpdate()
    {

        float x = Input.GetAxis("Horizontal") * speed;
        rb.velocity = new Vector2(x, rb.velocity.y);

        if (Input.GetKey("space") && canJump == true)
        {
            rb.velocity = new Vector2(rb.velocity.x, jump);
            canJump = false;
        }

        ChangeColor();

    }

    void ChangeColor()
    {

        if (Input.GetKey("j"))
        {
            color = "Red";
            this.gameObject.tag = color;
            sprite.color = Color.red;
        }
        if (Input.GetKey("k"))
        {
            color = "Blue";
            this.gameObject.tag = color;
            sprite.color = Color.blue;
        }
        if (Input.GetKey("l"))
        {
            color = "Yellow";
            this.gameObject.tag = color;
            sprite.color = Color.yellow;
        }

    }

    void OnCollisionEnter2D(Collision2D collision)
    {

        if (collision.gameObject.tag == "Ground")
        {
            canJump = true;
        }
        else
        {
            if (collision.gameObject.tag != this.gameObject.tag)
            {
                container.SetActive(true);
                int finalScore = FindObjectOfType<GameManager>().score;
                tmpUGUI.text = "Final Score: " + finalScore as string;
                box.isTrigger = true;
            }
        }

    }
}
