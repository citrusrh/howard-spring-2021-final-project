﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{

    public int score = 0;
    public int score_multiplier = 1;
    public float g_scale = 1f;

    public TextMeshProUGUI tmpUGUI;


    public void EndGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void FixedUpdate()
    {
        g_scale = 1f + GameObject.Find("Player").transform.position.y / 10;

        score_multiplier = ((int)GameObject.Find("Player").transform.position.y) / 10 + 1;
        score += 1 * score_multiplier;

        tmpUGUI.text = "Score: " + score as string;
    }

}
